/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.latihan4c;
import java.util.Scanner;
/**
 *
 * @author T I O
 */
public class Latihan4c {

    public static void main(String[] args) {
        Scanner input = new Scanner (System.in);
        String nama, prodi;
        char huruf;
        int nilai;
    System.out.println("Data Test");
    System.out.println("============================");
    System.out.print("Nama          :");
    nama = input.nextLine();
    System.out.print("Program Studi :");
    prodi = input.nextLine();
    System.out.print("Nilai         :");
    nilai = input.nextInt();
    if (nilai >=85 && nilai <=100){
        huruf = 'A';
    }
    else if (nilai >=70 && nilai <=85){
        huruf = 'B';
    }
    else if (nilai >=60 && nilai <=70){
        huruf = 'C';
    }
    else if (nilai >=50 && nilai <=60){
        huruf = 'D';
    }
    else {
        huruf = 'E';
    }
     System.out.print("Nilai Huruf   :"+huruf);
    }
}
