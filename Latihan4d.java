/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.latihan4d;
import java.util.Scanner;
/**
 *
 * @author T I O
 */
public class Latihan4d {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);    
        String nama, noplg;
        int pakai, biaya;   
        System.out.println("Perhitungan Biaya Pemakaian Air ");
        System.out.println("=================================");
        System.out.print("Nama         : "); 
        nama = input.nextLine();
        System.out.print("No Pelanggan : ");
        noplg = input.nextLine();
        System.out.print("Pemakaian Air: " );
        pakai = input.nextInt();
        if (pakai <= 10) {
            biaya = pakai * 1000;
        } else if (pakai <= 20) {
            biaya = 10 * 1000 + (pakai - 10) * 2000;
        } else {
            biaya= 10 * 1000 + 10 * 2000 + (pakai - 20) * 5000;
        }
    
        System.out.println("Biaya Pakai : " + biaya);
      
    }
}
    

